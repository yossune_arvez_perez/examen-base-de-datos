
--  script 6
SELECT distinct caj.Cajero, caj.NomApels
FROM Cajeros Caj
inner join venta vent on vent.cajero = caj.cajero
inner join productos prod on vent.producto = prod.producto
INNER JOIN Maquinas_registradoras maq on vent.maquina = maq.maquina and
 Vent.Producto = Prod.producto
AND Vent.Maquina = Maq.maquina
GROUP BY caj.Cajero
HAVING SUM(Precio) < cast(5000 as money)