-- Table: public."Cajeros"

-- DROP TABLE public."Cajeros";

CREATE TABLE Cajeros
(
    Cajero integer NOT NULL,
    NomApels varchar(255) COLLATE pg_catalog.default NOT NULL,
    CONSTRAINT Cajeros_pkey PRIMARY KEY (Cajero)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE public."Cajeros"
 --   OWNER to sa;

INSERT INTO Cajeros(
	Cajero, NomApels)
	VALUES (1, 'cajero 1')
			,(2, 'cajero 2')
			,(3, 'cajero 3')
			,(4, 'cajero 4');
			
------- tabla productos
-- Table: public."Productos"

-- DROP TABLE public."Productos";

CREATE TABLE Productos
(
    Producto integer NOT NULL,
    Nombre varchar(100) COLLATE pg_catalog.default NOT NULL,
    Precio money NOT NULL,
    CONSTRAINT Productos_pkey PRIMARY KEY (Producto)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE public."Productos"
 --  OWNER to sa;
 
 
 INSERT INTO Productos(
	Producto, Nombre, Precio)
	VALUES (1, 'aceite', 50)
			,(2, 'Limpia vidrio', 30)
			,(3, 'aditivo gasolina', 80)
			,(4, 'liquido de freno', 28.40);

--- ---- maquinas registradoras			

-- Table: public."Maquinas_Registradoras"

-- DROP TABLE public."Maquinas_Registradoras";

CREATE TABLE Maquinas_Registradoras
(
    Maquina integer NOT NULL,
    Piso integer NOT NULL,
    CONSTRAINT Maquinas_Registradoras_pkey PRIMARY KEY (Maquina)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE public."Maquinas_Registradoras"
 --   OWNER to sa;
 
 INSERT INTO Maquinas_Registradoras(
	Maquina, Piso)
	VALUES (1, 1)
			,(2, 2)
			,(3, 3)
			,(4, 3);
			
			
------ ventas  

-- Table: public."Venta"

-- DROP TABLE public."Venta";

CREATE TABLE Venta
(
    Cajero integer NOT NULL,
    Maquina integer NOT NULL,
    Producto integer NOT NULL,
    CONSTRAINT Cajero FOREIGN KEY (Cajero)
        REFERENCES Cajeros (Cajero) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT Maquina FOREIGN KEY (Maquina)
        REFERENCES Maquinas_Registradoras (maquina) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT Producto FOREIGN KEY (Producto)
        REFERENCES Productos (Producto) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ALTER TABLE public."Venta"
   -- OWNER to sa;
   
   
   
   
   INSERT INTO Venta(
	Cajero, Maquina, Producto)
	VALUES (1, 1, 1)
			,(1, 1, 2)
			,(1, 1, 3)
			,(2, 2, 1)
			,(2, 2, 2)
			,(2, 2, 4)
			,(2, 2, 3)
			,(3, 3, 1)
			,(3, 3, 2)
			,(3, 3, 3)
			,(3, 3, 4)
			,(4, 4, 1)
			,(4, 4, 2)
			,(4, 4, 3)
			,(4, 4, 4)
			,(1, 2, 1)
			,(1, 2, 2)
			,(1, 2, 3)
			,(1, 2, 4)
			,(2, 3, 1)
			,(2, 3, 2)
			,(2, 3, 3)
			,(2, 3, 4)
			,(2, 4, 1)
			,(2, 4, 2)
			,(2, 4, 3)
			,(2, 4, 4);			