-- script 5

SELECT caj.Cajero, caj.NomApels, SUM(prod.Precio)
FROM Cajeros Caj
inner join venta vent on vent.cajero = caj.cajero
inner join productos prod on vent.producto = prod.producto
GROUP BY Caj.cajero, NomApels 